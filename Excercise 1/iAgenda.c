#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <unistd.h>
#include <string.h>
#include <errno.h>
#include <signal.h>

//********************************************global defines*************************************
#define done 1
#define undone 0
#define no_of_tasks 9

//*******************************************global variables*************************************
int speed = 1, minutes = 10, hours = 12;
struct activity {
	char name[20];
	unsigned int start_time;
	unsigned int end_time;
	unsigned int status;	//0: undone, 1: done
	unsigned int started_alarmed;
	unsigned int ending_alarmed;
};
struct activity act[no_of_tasks];

//*********************************private functions*********************************************
/*
 * returns the index of requested activity at the given time.
 * Input: time value converted to minutes
 * Output: index of the activity at the given time, or 100 if not found
 * */
unsigned int which_task_due(unsigned int act_time){
	unsigned int i;
	for (i=0;i<no_of_tasks;i++){
		if ((act_time >= act[i].start_time) && (act_time <= act[i].end_time)){
			return i;
		}
	}
	return 100;	//100 means not found
}
/*
 * returns the index of task currently reaching end time - 10 minutes.
 * Input: time value converted to minutes
 * Output: index of the activity or 100 if not found
 * */
unsigned int which_task_ending(unsigned int t){
	unsigned int i, x=100;
	for (i=0;i<no_of_tasks;i++){
		if (t > (act[i].end_time - 10)){
			x =  i;
		}
	}
	return x;
}

/* delay in seconds */
void delay_s(unsigned int t){
	clock_t t0;
	t0 = clock();
	while (((clock() - t0)/CLOCKS_PER_SEC) < t);
}

/*
	prints the activity for the given time on the console
	Input: activity hour and minute
	reuturn: none
*/
void display_activity(unsigned int hr, unsigned min){
	unsigned int task_index;
	char ans[10];

	task_index = which_task_due(hr*60 + min);
	if (task_index == 100) {
		printf("Nothing to do at that time, have fun.\n"); fflush(stdout);
	}
	else {
		printf("%s time\n", act[task_index].name); fflush(stdout);
		if (act[task_index].status == done){
			delay_s(3);
			printf("Chill, you already had %s\n", act[task_index].name); fflush(stdout);

		}
		else {
			delay_s(3);
			printf("Are you having %s?\n", act[task_index].name); fflush(stdout);
			fflush(stdin);
			scanf(" %s", &ans);
			if (strcmp(ans,"yes") == 0) {
				act[task_index].status = done;
				printf("%s DONE\n", act[task_index].name); fflush(stdout);
			}
		}
	}
}
/*	function to initialize the activitiies structure with 9 different activities
(for grandma not to get bored :))
*/
void schedule_init(){
	unsigned int i;
	unsigned int start_hr, start_min, end_hr, end_min;

	strcpy(act[0].name,"Breakfast       ");
	strcpy(act[1].name,"Drink water     ");
	strcpy(act[2].name,"Morning medicine");
	strcpy(act[3].name,"Feed the dog    ");
	strcpy(act[4].name,"Lunch           ");
	strcpy(act[5].name,"Drink water     ");
	strcpy(act[6].name,"Dinner          ");
	strcpy(act[7].name,"Evening medicine");
	strcpy(act[8].name,"Sleep           ");
	
	//activites are arbitarily distributed on the day, duration of all set to 60 minutes
	for (i=0;i<no_of_tasks;i++){
		act[i].start_time = 360 + i*106;
		act[i].end_time = act[i].start_time + 60;
		act[i].status = undone;
	}
	
	for (i=0;i<no_of_tasks;i++){
		start_hr = act[i].start_time / 60;
		start_min = act[i].start_time - (60*start_hr);
		end_hr = act[i].end_time / 60;
		end_min = act[i].end_time - (60*end_hr);
		printf("%s starts at %d:%d ends at %d:%d and is %d\n",act[i].name,start_hr, start_min, end_hr, end_min, act[i].status);
	}

}
/*	Timer callbakc function
triggers every second to check if an activity is beginning or ending
*/
void timer_callback(int signum)
{
	unsigned int task_index, i;
    static unsigned long tick = 0;

    if (tick < (86400-speed)){	//no of seconds / day = 24*60*60 = 86400
        tick += speed;	//if speed factor = 1, increment every 1 second (1 tick = 1 second)
    }
    else {
        tick = (speed - (86400 - tick));
		for (i=0;i<no_of_tasks;i++){
			act[i].started_alarmed = 0;	//re-init with the beginning of a new day
			act[i].ending_alarmed = 0;
		}
    }

    hours = tick / (60*60);
    minutes = (tick - hours *3600)/60;

	//alarm at the start of a due task
    task_index = which_task_due(hours*60 + minutes);
	if (task_index != 100) {
		if (act[task_index].started_alarmed == 0){
			printf("\nIt's %d:%d -  %s time.", hours, minutes, act[task_index].name); fflush(stdout);
		}
		act[task_index].started_alarmed = 1;	//flag to prevent more than once alarming for the same task
	}

	//alarm 10 minutes before the end of an undone task
	task_index = which_task_ending((hours*60 + minutes));
	if ((task_index != 100) && (act[task_index].status == undone)) {
		if (act[task_index].ending_alarmed == 0){
			printf("\nIt's %d:%d - %s ending soon.", hours, minutes, act[task_index].name); fflush(stdout);
		}
		act[task_index].ending_alarmed = 1;

	}
    alarm(1);	//re-init alarm triggering
}

int main(int argc, char *argv[])
{
	char cmd[50], cmd_shadow[50];
	char *ptr;
	unsigned int hr, min;

	signal(SIGALRM, timer_callback);
	alarm(1);
	schedule_init();

	while (1){
		/* Command Parser:
		 * Inputs:
		 * 	1- Speed Factor: syntax: speed=x , where x is the speed factor (1: normal, 2: double speed, etc.)
		 * 	2- now: to list the activity at the moment
		 * 	3- time value (HH:MM): to list activity at the given time (ex: 10:13)
		 *  4- exit: to end the application and return to OS 
		 */
		//get input string from terminal
		printf("\n>"); fflush(stdout);
		scanf(" %50[^\n]s", cmd);

		//parse speed value
		if (strstr(cmd,"speed") != NULL){
			ptr = strchr(cmd,'=');
			if (ptr != NULL){
				speed = atoi(++ptr);
				printf("Speed Factor:%d\n", speed);
			}
			else {
				printf("Invalid command\n");
			}
		}

		//parse the now string
		else if (strstr(cmd,"now") != NULL){
			printf("It's %d:%d now.\n", hours, minutes);
			display_activity(hours,minutes);
		}

		//parse time value
		else if ((ptr = strchr(cmd,':'))){
			min = atoi(++ptr);
			strcpy(cmd_shadow, cmd);
			strtok (cmd_shadow,":");
			if (cmd_shadow != NULL){
				hr = atoi(cmd_shadow);
			}
			if ((hr > 23) || (min > 59)) {
				printf("Invalid Time Value");
			}
			else {
				printf("Time: %d:%d\n", hr, min);
				display_activity(hr,min);
			}
		}
		else if (strstr(cmd,"exit") != NULL){
			printf("Exiting\n");
			exit(0);
		}
		else {
			printf("Invalid command");
		}

	}
	return 0;
}
