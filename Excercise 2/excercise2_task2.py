import RPi.GPIO as GPIO
import time

connector1 = (2,3,4,5,6,7)          #connector #1 pins
connector2 = (8,9,10,11,12,13)      #connector #2 pins
connector3 = (14,15,16,17,18,19)    #connector #3 pins
LED = (20)							#indicator LED on pin #20

pattern1 = [1,1,1,1,1,0]
pattern2 = [1,1,1,1,0,1]
pattern3 = [1,1,1,0,1,1]
pattern4 = [1,1,0,1,1,1]
pattern5 = [1,0,1,1,1,1]
pattern6 = [0,1,1,1,1,1]
patterns = [pattern1,pattern2,pattern3,pattern4,pattern5,pattern6]      #rotating zero patterns

GPIO.setmode(GPIO.BCM)              #GPIO pin numbering mode
GPIO.setup(connector1, GPIO.OUT)    #connector #1 pins as output
GPIO.setup(LED, GPIO.OUT)           #status LED pin as output
GPIO.setup(connector2, GPIO.IN, pull_up_down=GPIO.PUD_UP)   #input with pullups
GPIO.setup(connector3, GPIO.IN, pull_up_down=GPIO.PUD_UP)   #input with pullups

j = 0
status = True

while (j<6):        #repeat 6 times
    DI2 = []		#initialize list for digital input from connector #2
    DI3 = []		#initialize list for digital input from connector #3
    for i in patterns:
        GPIO.output(connector1, i)      #output rotating "0" pattern to connector #1
        time.sleep(0.5)     			#wait until signals stabilize
        for pin in connector2:  		#read connector #2 pins
            DI2.append(GPIO.input(pin))
        for pin in connector3:  		#read connector #3 pins
            DI3.append(GPIO.input(pin))
        if (DI2 == i) and (DI3 == i):   #if read-back patterns match the output pattern  	
            print("Pin #" + str(j) + "is OK\n")	#then pin is OK
        else:
            status = False   			#else, pin is not OK (either open, shorted with another pin, or interchanged with another pin)
            print("Pin #" + str(j) + "is faulty\n")
    j += 1      						#continue to next pattern

if status:
    GPIO.output(LED,1)      #on success, turn LED on
    print("Cable test passed")
else:
    GPIO.output(LED,0)      #on failure, turn LED off
    print("Cable test failed")

GPIO.cleanup()  #cleanup before leaving


